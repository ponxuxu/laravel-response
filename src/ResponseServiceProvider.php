<?php

namespace Pon\LaravelResponse;

use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{

    public function boot()
    {

    }

    /**
     * 注册服务
     */
    public function register(): void
    {
        $this->app->singleton(Response::class, function () {
            return new Response();
        });
    }

}
