<?php

namespace Pon\LaravelResponse\Facades;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Facade;

/**
 * @method static JsonResponse success(array $data = [], string $msg = 'success', int $code = 200)
 * @method static JsonResponse failure(array $data = [], string $msg = 'failure', int $code = 400)
 * @method static JsonResponse paginateResource(JsonResource $resource, string $msg = 'success', int $code = 200)
 * @method static JsonResponse resource(JsonResource $resource, string $msg = 'success', int $code = 200)
 */
class Response extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \Pon\LaravelResponse\Response::class;
    }
}
