<?php

namespace Pon\LaravelResponse;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Pon\LaravelResponse\Contracts\ResponseFormat;

class Response implements ResponseFormat
{
    /**
     * @desc 分页资源数据返回
     * @param JsonResource $resource
     * @param string $msg
     * @param int $code
     * @return JsonResponse
     */
    public function paginateResource(JsonResource $resource, string $msg = 'success', int $code = 200): JsonResponse
    {
        $resolve = $resource->resolve();
        $data = [
            'rows' => $resolve['data'],
            'current_page' => $resolve['current_page'],
            'per_page' => $resolve['per_page'],
            'total' => $resolve['total']
        ];

        return $this->jsonResponse($data, $msg, $code);
    }

    /**
     * @desc Json响应
     * @param array|JsonResource $data
     * @param string $msg
     * @param int $code
     * @return JsonResponse
     */
    public function jsonResponse(array $data = [], string $msg = '', int $code = 0): JsonResponse
    {
        $payload = [
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        ];

        return new JsonResponse($payload, $code);
    }

    /**
     * @desc 资源数据返回
     * @param JsonResource $resource
     * @param string $msg
     * @param int $code
     * @return JsonResponse
     */
    public function resource(JsonResource $resource, string $msg = 'success', int $code = 200): JsonResponse
    {
        $data = $resource->resolve();

        return $this->jsonResponse($data, $msg, $code);
    }

    /**
     * @desc 成功返回
     * @param array $data
     * @param string $msg
     * @param int $code
     * @return JsonResponse
     */
    public function success(array $data = [], string $msg = 'success', int $code = 200): JsonResponse
    {
        return $this->jsonResponse($data, $msg, $code);
    }

    /**
     * @desc 失败返回
     * @param array $data
     * @param string $msg
     * @param int $code
     * @return JsonResponse
     */
    public function failure(array $data = [], string $msg = 'failure', int $code = 400): JsonResponse
    {
        return $this->jsonResponse($data, $msg, $code);
    }
}
