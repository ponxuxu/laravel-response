<?php

namespace Pon\LaravelResponse\Contracts;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

interface ResponseFormat
{

    /**
     * @desc 成功返回结构
     * @param array $data 返回数据
     * @param string $msg 返回信息
     * @param int $code 返回状态码
     * @return JsonResponse
     */
    public function success(array $data = [], string $msg = 'success', int $code = 200): JsonResponse;

    /**
     * @desc 失败返回结构
     * @param array $data 返回数据
     * @param string $msg 返回信息
     * @param int $code 返回状态码
     * @return JsonResponse
     */
    public function failure(array $data = [], string $msg = 'failure', int $code = 400): JsonResponse;

    /**
     * @desc 资源数据返回
     * @param JsonResource $resource
     * @param string $msg
     * @param int $code
     * @return JsonResponse
     */
    public function resource(JsonResource $resource, string $msg = 'success', int $code = 200): JsonResponse;

    /**
     * @desc 分页资源数据返回
     * @param JsonResource $resource
     * @param string $msg
     * @param int $code
     * @return JsonResponse
     */
    public function paginateResource(JsonResource $resource, string $msg = 'success', int $code = 200): JsonResponse;
}
